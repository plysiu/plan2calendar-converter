var convert = require('xml-js');
var request = require('request')
module.exports.XMLtoJSON = (xml) => convert.xml2json(xml, { compact: true, spaces: 4 })

module.exports.URLtoJSON = (url) => new Promise((res, rej) =>
    request.get(url, (error, response, body) => res(this.XMLtoJSON(body))))

module.exports.JSONtoICS = (json, url) => new Promise((res, rej) => {


    var plan = JSON.parse(json)
    var name = plan['plan-zajec']._attributes.nazwa

    var typ = plan['plan-zajec']._attributes.typ._text
    var interval = plan['plan-zajec']._attributes.od + " - " + plan['plan-zajec']._attributes.do
    var lessons = [].concat(plan['plan-zajec'].zajecia)
    const ical = require('ical-generator');


    let cal = ical({ domain: 'plan-zajec', name: name });
    cal.timezone('Europe/Berlin');
    cal.ttl(60 * 60);

    lessons.forEach(element => {
        let organizer, room
        if (typ == "N") {
            //room, group
            organizer = {
                name: element.grupa._text + " ",
                email: ""
            }
            room = element.sala._text
        }
        if (typ == "G") {
            //room tutor
            organizer = {
                name: element.nauczyciel._text + " ",
                email: ""
            }
            room = element.sala._text
        }
        if (typ == "S") {
            //tutor group
            organizer = {
                name: element.nauczyciel._text + " " + element.grupa._text,
                email: ""
            }
            room = name
        }



        var event_name = element.przedmiot._text
        var event_type = element.typ._text

        var termin = element.termin._text;
        var from = element['od-godz']._text;
        var to = element['do-godz']._text;

        var start = new Date(termin + " " + from)
        var end = new Date(termin + " " + to)

        cal.createEvent({
            start: start,
            end: end,
            summary: event_type + ' ' + event_name,
            description: "",
            organizer: organizer,
            location: room,
            url: url
        });
    });

    var filename = name + " " + interval + ".ics"
    cal.save(__dirname + "/calendars/" + filename, c => { res(filename) })
})
