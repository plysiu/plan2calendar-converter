const express = require('express')
const converter = require('./converter')
const app = express()
const port = 3000

var fs = require('fs');
var dir = './calendars';

if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
}

app.get('/', (req, res) => {
    var url = `http://planzajec.uek.krakow.pl/index.php?typ=${req.query.typ}&okres=${req.query.okres}&id=${req.query.id}&xml`
    if (req.query.ics)
        converter.URLtoJSON(url)
            .then(json =>
                converter.JSONtoICS(json, url)
                    .then(ics => res.download(__dirname + '/calendars/' + ics, ics))
                    .catch(err => res.send(err))
            )
            .catch(err => res.send(err))
    else
        converter.URLtoJSON(url)
            .then(json => res.send(json))
            .catch(err => res.send(err))
})
app.listen(port, '0.0.0.0')